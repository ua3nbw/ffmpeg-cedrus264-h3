# README #

h264 encoder support for Allwinner H3 CPU
https://ua3nbw.ru/files/ffmpeg-cedrus264-H3.tar.gz
### What is this repository for? ###

These are modified sources of the port of FFmpeg for Cedrus originally located here: https://github.com/stulluk/FFmpeg-Cedrus To use these sources checkout original sources and replace file FFmpeg-root-dir/libavcodec/cedrus264.c with one from this repository, replace folder FFmpeg-root-dir/libavcodec/arm/sunxi with folder from this repository.
https://github.com/uboborov/ffmpeg_h264_H3
### How do I get set up? ###
FFMpeg for Cedrus on Allwinner devices with sunxi kernel Based on increadible work with jemk and alcantor, we managed to update FFmpeg to include cedrus264 hardware encoder. Now it encodes I and P frames, and support resolutions up to 1080p
https://github.com/stulluk/FFmpeg-Cedrus